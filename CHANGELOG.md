#### 1.4.4 (2016-8-30)

##### Chores

* **package:** Update dependencies ([28065ae4](https://github.com/lgaticaq/tracking-parser/commit/28065ae44dd984971c1d2bcced342b27758da36b))

#### 1.4.3 (2016-8-30)

##### Chores

* **parser:** Simplified getImei ([af8a1575](https://github.com/lgaticaq/tracking-parser/commit/af8a15757f4fddf183f98973a449070e688dff53))
* **package:**
  * Update dependencies ([a3f52332](https://github.com/lgaticaq/tracking-parser/commit/a3f5233206b4a461791ebfa2a92cdfc53856a41f))
  * update eslint to version 3.4.0 ([66e876c9](https://github.com/lgaticaq/tracking-parser/commit/66e876c92879bdb1ee74d795511606bd66e8b593))

#### 1.4.2 (2016-8-25)

##### Bug Fixes

* **test:** Fix queclink test ([31db6ad9](https://github.com/lgaticaq/tracking-parser/commit/31db6ad95cc12906580c0bfa5fa066f7eb32df25))

#### 1.4.1 (2016-8-25)

##### Chores

* **package:** Update queclink-parser ([67c94cb9](https://github.com/lgaticaq/tracking-parser/commit/67c94cb96567240662fd481fb0a86d8a636155a7))

### 1.4.0 (2016-8-25)

##### Chores

* **package:**
  * Update dependencies ([1ae3fb34](https://github.com/lgaticaq/tracking-parser/commit/1ae3fb344be572b0dbe5579aa9890331f3846403))
  * update queclink-parser to version 0.2.0 ([990cdeac](https://github.com/lgaticaq/tracking-parser/commit/990cdeacd6a8cd15ffdc8e871903bb8f4acd4c89))

#### 1.3.2 (2016-8-17)

##### Chores

* **package:** Update cellocator-parser to 2.2.1 ([a03f645c](https://github.com/lgaticaq/tracking-parser/commit/a03f645cbe68444ebfac09e28874de31bdde5c0b))

#### 1.3.1 (2016-8-17)

##### Chores

* **package:**
  * update eslint to version 3.3.1 ([7e5d7e71](https://github.com/lgaticaq/tracking-parser/commit/7e5d7e718539de9f050b29195e38136dbf7f2a30))
  * update eslint to version 3.3.0 ([914c95c0](https://github.com/lgaticaq/tracking-parser/commit/914c95c068cc8cc13bdaf0f4988cd21f1dfd8026))

##### New Features

* **package:** Update cellocator-parser to 2.2.0 ([026bca7e](https://github.com/lgaticaq/tracking-parser/commit/026bca7e77f9e20560f6c5f68ba299f18f6811e6))

### 1.3.0 (2016-8-11)

##### Chores

* **package:**
  * Update dependencies ([bfa78c79](https://github.com/lgaticaq/tracking-parser/commit/bfa78c792773229d9fdaf75c3c4db9f7e02526ba))
  * update tz-parser to version 2.4.4 ([1a223d56](https://github.com/lgaticaq/tracking-parser/commit/1a223d56970a729e7eab66b11420a7f9f8fce8b6))
  * update mocha to version 3.0.2 ([b7cf7463](https://github.com/lgaticaq/tracking-parser/commit/b7cf7463f908b36e8ab8dc64406455feb71fb5d1))
  * update mocha to version 3.0.1 ([c8f13831](https://github.com/lgaticaq/tracking-parser/commit/c8f138315c312bdb6eb59b38e3874412525a4558))
  * update meitrack-parser to version 0.3.4 ([300a6370](https://github.com/lgaticaq/tracking-parser/commit/300a63706e0919c883a5a3619f59357ccea304b1))
  * update eslint to version 3.2.2 ([3fa306c9](https://github.com/lgaticaq/tracking-parser/commit/3fa306c9af63b57b7adceffba39ef7ea38d8dd91))
  * update mocha to version 3.0.0 ([d6caecc6](https://github.com/lgaticaq/tracking-parser/commit/d6caecc6cf994a7bdb4bb0cb2e24c3632c766d5c))
  * update eslint to version 3.2.0 ([fa202234](https://github.com/lgaticaq/tracking-parser/commit/fa202234800b29696eaee3d3098b445e1b9a8523))

##### New Features

* **parser:** Add queclink-parser ([d0ed06a0](https://github.com/lgaticaq/tracking-parser/commit/d0ed06a018cda55eb8ab2077549a188d95ae3c4d))

##### Bug Fixes

* **test:** Fix meitrack parseCommand test ([309d85d9](https://github.com/lgaticaq/tracking-parser/commit/309d85d9b759788ab5359e179b17052f046233a1))

#### 1.2.5 (2016-7-22)

##### Chores

* **package:**
  * Update coveralls ([5b02ed5f](https://github.com/lgaticaq/tracking-parser/commit/5b02ed5fa3b8056b69e90d3b81bad2dddcec7dc4))
  * update eslint to version 3.1.1 ([128dc302](https://github.com/lgaticaq/tracking-parser/commit/128dc302a5fad5c8e3b9019f4822fee92c1ed4c6))

##### Bug Fixes

* **parser:** Fix get device in getRebootCommand. ([1dcbd01a](https://github.com/lgaticaq/tracking-parser/commit/1dcbd01a44c6d1cc7a1e1989f29eb72441047858))

#### 1.2.4 (2016-7-8)

##### Bug Fixes

* **src:** Fix setCache ([c964fe68](https://github.com/lgaticaq/tracking-parser/commit/c964fe68143ecfdfeb5ac4aedde7885d1dbc8a3d))

#### 1.2.3 (2016-7-8)

##### Chores

* **cellocator:** Change parser to new api of cellocator. ([d01c2e4f](https://github.com/lgaticaq/tracking-parser/commit/d01c2e4fde214272b3c02583d0703e9dd8499b64))
* **package:**
  * update cellocator-parser to version 2.0.0 ([150da7e9](https://github.com/lgaticaq/tracking-parser/commit/150da7e9814b4c73d1c0fca67a409aa141f86c18))
  * update eslint to version 3.0.1 ([bee7a781](https://github.com/lgaticaq/tracking-parser/commit/bee7a7811b81e973500e026284a63da84ffdee60))
  * update eslint to version 3.0.0 ([57df23bf](https://github.com/lgaticaq/tracking-parser/commit/57df23bfda034dc8f721f49039c3f47ebd84d9a0))

#### 1.2.2 (2016-6-24)

##### Chores

* **package:**
  * update tz-parser to version 2.4.3 ([60f0c9f2](https://github.com/lgaticaq/tracking-parser/commit/60f0c9f206acc9951d643d228185aeb0c2c620cb))
  * Update tz-parser ([ce55532f](https://github.com/lgaticaq/tracking-parser/commit/ce55532fb074d0dca7c2efd6e3a1569c74433c41))
  * update cellocator-parser to version 1.0.2 ([760f1db8](https://github.com/lgaticaq/tracking-parser/commit/760f1db842ac3830038cc86d5a8c5ea397a302c7))
  * update cellocator-parser to version 1.0.1 ([48ea605d](https://github.com/lgaticaq/tracking-parser/commit/48ea605dfd5e357c000d0b909021b30c44e5f67f))
  * update cellocator-parser to version 1.0.0 ([6a9255c0](https://github.com/lgaticaq/tracking-parser/commit/6a9255c0aeff63162474c0d9eafc69b035f6fea4))
  * update simple-reverse-geocoder to version 1.2.2 ([1ff6c45a](https://github.com/lgaticaq/tracking-parser/commit/1ff6c45a8f269b2793b417b4beb07efae3f00fb3))
  * update cellocator-parser to version 0.2.2 ([50caab30](https://github.com/lgaticaq/tracking-parser/commit/50caab305f4c25a1c7b12c05568dbb31e4b8c503))

##### New Features

* **cellocator:** Add setClient in setCache ([da87edae](https://github.com/lgaticaq/tracking-parser/commit/da87edaef8ef5ac086785b41ae121ba5425a1c02))

#### 1.2.1 (2016-6-22)

##### Chores

* **package:** Update cellocator ([fc37e726](https://github.com/lgaticaq/tracking-parser/commit/fc37e726bdc3489e429634e5fef98d7f112d5a9a))

### 1.2.0 (2016-6-22)

##### Chores

* **package:**
  * Update cellocator-parser ([39a9e93b](https://github.com/lgaticaq/tracking-parser/commit/39a9e93bab25a3b25b71bafab49f161cd922e1c3))
  * update cellocator-parser to version 0.2.0 ([ad2810d5](https://github.com/lgaticaq/tracking-parser/commit/ad2810d51fe1f14c9009aa3ac3f5da6c1263be0c))

##### New Features

* **commands:** Add get cellocator ACK ([2d24ef33](https://github.com/lgaticaq/tracking-parser/commit/2d24ef3354ef2590c450f0739dded3707555c9e7))

#### 1.1.2 (2016-6-22)

##### Chores

* **package:** Update cellocator-parser ([497f9afa](https://github.com/lgaticaq/tracking-parser/commit/497f9afa7fd91f989fa51e6db0a044102cf73720))

#### 1.1.1 (2016-6-21)

##### Chores

* **package:**
  * update cellocator-parser to version 0.1.1 ([6f82fcce](https://github.com/lgaticaq/tracking-parser/commit/6f82fcce708f0a671f7a965f7d611030d14302d4))
  * Update dependencies ([f69e34aa](https://github.com/lgaticaq/tracking-parser/commit/f69e34aa157f7798c054aa64f4cd54d018c60953))
  * update eslint to version 2.13.1 ([1592bc79](https://github.com/lgaticaq/tracking-parser/commit/1592bc79ca8c53e70c999c48e5c029b856b75e9d))

##### Bug Fixes

* **parser:** Fix cellocator getImei ([5c5249db](https://github.com/lgaticaq/tracking-parser/commit/5c5249dbe548a2e1bcc6c9869cafc58c154e9281))

### 1.1.0 (2016-6-17)

##### Chores

* **package:**
  * Add keyword cellocator ([0cfee5f1](https://github.com/lgaticaq/tracking-parser/commit/0cfee5f1d703fcf17f853bd86b6d55ec0cf96e92))
  * Update dependencies ([132341cb](https://github.com/lgaticaq/tracking-parser/commit/132341cb05cf6872f7c42eccd570caf8c25000ed))
* **parser:** Add cellocator parser ([ebd533c8](https://github.com/lgaticaq/tracking-parser/commit/ebd533c877a1c0d579500c7515bdde8941369390))

##### Documentation Changes

* **readme:** Add parser availables ([52b5694b](https://github.com/lgaticaq/tracking-parser/commit/52b5694b463ebbcd9c323ad73d511edc6b942a5d))

##### Bug Fixes

* **src:** Fix get arguments in addLoc and parse ([8f00c419](https://github.com/lgaticaq/tracking-parser/commit/8f00c41990e38408898bf307a356feab5e8c3308))

## 1.0.0 (2016-6-16)

##### Chores

* **package:**
  * Upgrade simple-reverse-geocoder ([194448e1](https://github.com/lgaticaq/tracking-parser/commit/194448e1b562c810d74bcb998eefa432cda74b34))
  * Add code climate and generate-changelog ([8aa516af](https://github.com/lgaticaq/tracking-parser/commit/8aa516af2e3185468f4c80b044f702ca80484917))
  * update simple-reverse-geocoder to version 1.2.0 ([e4a7b481](https://github.com/lgaticaq/tracking-parser/commit/e4a7b4817a29285dee9fdecde318cde8707a3270))
  * update meitrack-parser to version 0.3.3 ([20c9dacd](https://github.com/lgaticaq/tracking-parser/commit/20c9dacd2097917ea3b3aa0f1b39047819704795))
  * update tz-parser to version 2.4.2 ([b3ecddab](https://github.com/lgaticaq/tracking-parser/commit/b3ecddab6f9d813e0fec5c52e9e63e9751df5324))
  * update eslint to version 2.12.0 ([7094c0d8](https://github.com/lgaticaq/tracking-parser/commit/7094c0d86fe21a07f25e8f728807a6dc5af18d3e))
  * update eslint to version 2.11.1 ([963421c9](https://github.com/lgaticaq/tracking-parser/commit/963421c96850cc7684c3b3ad3e6def2e39301e17))
  * update eslint to version 2.11.0 ([43c186e8](https://github.com/lgaticaq/tracking-parser/commit/43c186e8f4b332bf491a9635b76f3902b333a653))
  * update eslint to version 2.10.2 ([5e49e0a6](https://github.com/lgaticaq/tracking-parser/commit/5e49e0a62ef58dc3f7472657522e3f3c5b227157))
  * update eslint to version 2.10.1 ([d70e11c1](https://github.com/lgaticaq/tracking-parser/commit/d70e11c17654117a075f8eecee8d6fcc5ab43c11))
  * update eslint to version 2.10.0 ([c3f4a72a](https://github.com/lgaticaq/tracking-parser/commit/c3f4a72ac98b7352d8e403fbbd9bc190e0d3ee75))
  * update babel-core to version 6.7.7 ([2206d13e](https://github.com/lgaticaq/tracking-parser/commit/2206d13ecadd0a93199b962125b15fbedeec747c))
  * update eslint to version 2.9.0 ([6e1fe53d](https://github.com/lgaticaq/tracking-parser/commit/6e1fe53d8829eba444f721161f5801578db8a3c9))
  * update babel-cli to version 6.7.7 ([66101ed7](https://github.com/lgaticaq/tracking-parser/commit/66101ed7b95976653c6d907db4b9a9ea0a27aac1))
  * update dependencies ([2fcf52a7](https://github.com/lgaticaq/tracking-parser/commit/2fcf52a7967cc55cf67518b9a7b2fbb044145ba3))
* **node:** Upgrade node version ([2f9d3564](https://github.com/lgaticaq/tracking-parser/commit/2f9d356424bca90ddc5801f298270688877c1fe9))
* **src:** Remove babel ([f4bff65c](https://github.com/lgaticaq/tracking-parser/commit/f4bff65c6f49cda40c5feeb1ae75cc92ee3ad272))

##### Documentation Changes

* **changelog:** Add changelog ([f9776310](https://github.com/lgaticaq/tracking-parser/commit/f97763100be8b02087e184143ba5c3727b4bd764))

#### 0.4.3 (2016-04-12)

* Update meitrack-parser ([fc40304](https://github.com/lgaticaq/tracking-parser/commit/fc40304))

#### 0.4.2 (2016-04-11)

* Update meitrack-parser ([93bf57a](https://github.com/lgaticaq/tracking-parser/commit/93bf57a))

#### 0.4.1 (2016-04-11)

* Fix meitrack test ([4d09919](https://github.com/lgaticaq/tracking-parser/commit/4d09919))
* Update dependencies ([f09f517](https://github.com/lgaticaq/tracking-parser/commit/f09f517))

### 0.4.0 (2016-04-01)

* Add getRebootCommand ([ed1a78d](https://github.com/lgaticaq/tracking-parser/commit/ed1a78d))
* Update dependencies ([7c8ece2](https://github.com/lgaticaq/tracking-parser/commit/7c8ece2))

#### 0.3.1 (2016-03-31)

* Update tz-parser ([2fe6705](https://github.com/lgaticaq/tracking-parser/commit/2fe6705))

### 0.3.0 (2016-03-29)

* Add parse command from meitrack ([5dffaca](https://github.com/lgaticaq/tracking-parser/commit/5dffaca))
* Update dependencies ([cfe46fd](https://github.com/lgaticaq/tracking-parser/commit/cfe46fd))

#### 0.2.1 (2016-03-24)

* Fix addAddress ([b1c5e42](https://github.com/lgaticaq/tracking-parser/commit/b1c5e42))
* Remove currentData ([3f3cfa6](https://github.com/lgaticaq/tracking-parser/commit/3f3cfa6))
* Update dependencies ([e50f396](https://github.com/lgaticaq/tracking-parser/commit/e50f396))

### 0.2.0 (2016-03-15)

* Add parseCommand ([07d2711](https://github.com/lgaticaq/tracking-parser/commit/07d2711))
* Change licence year ([58e1d89](https://github.com/lgaticaq/tracking-parser/commit/58e1d89))
* Update dependencies ([22ee78b](https://github.com/lgaticaq/tracking-parser/commit/22ee78b))

#### 0.1.1 (2016-03-08)

* Update dependencies ([eabdbd6](https://github.com/lgaticaq/tracking-parser/commit/eabdbd6))

### 0.1.0 (2016-03-02)

* Add parse ([9a539d6](https://github.com/lgaticaq/tracking-parser/commit/9a539d6))
* Trigger ([e22cafa](https://github.com/lgaticaq/tracking-parser/commit/e22cafa))
* Update dependencies ([7ac0da1](https://github.com/lgaticaq/tracking-parser/commit/7ac0da1))
* Update example ([da6f5ea](https://github.com/lgaticaq/tracking-parser/commit/da6f5ea))

#### 0.0.1 (2016-03-02)

* first commit ([dc21f94](https://github.com/lgaticaq/tracking-parser/commit/dc21f94))
